import argparse
from datetime import tzinfo, timedelta, datetime
import time
import re
#import vlc

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# DEFAULT VALUES ----------------------
DEFAULT_AFTER_HOUR = "08:00"
DEFAULT_BEFORE_HOUR = "22:00"
DEFAULT_CHECK_INTERVAL_IN_SEC = 30
choosen_city = 0
choosen_spec = 0
choosen_cons = 0
book_dict = {}
# DEFAULT VALUES ----------------------

def login(driver, login, password):
    driver.minimize_window()
    print("* INF: Enter enelmed login screen ...")
    driver.get("https://online.enel.pl/lp/")
    time.sleep(10)
    button = driver.find_element_by_xpath("/html/body/div/header/div/div[1]/p/button[1]")
    button.click()

    print("* INF: Logging in ...")
    login_form = driver.find_element_by_xpath('//*[@id="Login"]')
    password_form = driver.find_element_by_xpath('//*[@id="Password"]')

    login_form.send_keys(str(login))
    password_form.send_keys(str(password))

    time.sleep(2)

    tick_button = driver.find_element_by_xpath('//*[@id="IsAcceptedRule"]')
    tick_button.click()

    time.sleep(2)
    zaloguj_button = driver.find_element_by_xpath("/html/body/div[1]/section/div[1]/div[1]/form/div/div[2]/div[4]/a")
    zaloguj_button.click()

    time.sleep(2)
    try:
        privacy_policy = driver.find_element_by_class_name("js-close-popover")
    except: 
        print("* INF: No popup to click - skipping ")
        pass
    else:
        print("* INF: Popup to click")
        privacy_policy.click()

    time.sleep(2)
    fs_name = driver.find_element_by_xpath("/html/body/header/div/div/div[2]/div[3]/button/span[1]").get_attribute("innerHTML")
    if fs_name:
        print("* INF: You are logged as: " + fs_name)
        time.sleep(10)
        pass
    else:
        print("* ERR: You are not logged")
        raise

def choose_visit(driver, data_dstart, data_dto, english):
    global choosen_city
    global choosen_cons
    global choosen_spec

    print("* INF: Enter page book visit ...")
    driver.get("https://online.enel.pl/Visit/New")

    # choose city 
    time.sleep(10)
    city=driver.find_element_by_xpath('//*[@id="City"]').find_elements_by_tag_name("option")
    if not choosen_city:
        print("* INF: Please choose CITY:")
        for option in city:
            if option.get_attribute("value"):
                print(option.get_attribute("value")+'\t - '+option.get_attribute("text"))
        choosen_city = input("Enter number: ")

    for option in city:
        option.click()
        if option.get_attribute("value") == choosen_city:
            break

    ## choose type of visit
    time.sleep(5)
    consultation=driver.find_element_by_xpath('//*[@id="ListOfTypes"]').find_elements_by_tag_name('option')
    if not choosen_cons:
        print("* INF: Please choose type of visit:")
        for option in consultation:
            if option.get_attribute("value"):
                print(option.get_attribute("value")+'\t - '+option.get_attribute("text"))
        choosen_cons = input("Enter number: ")

    for option in consultation:
        option.click()
        if option.get_attribute("value") == choosen_cons:
            break

    ## choose consultation
    time.sleep(5)
    specialization=driver.find_element_by_xpath('//*[@id="ListOfServices"]').find_elements_by_tag_name('option')
    if not choosen_spec:
        print(" * INF: Please choose specialization:")
        for option in specialization:
            if option.get_attribute("value"):
                print(option.get_attribute("value")+'\t - '+option.get_attribute("text"))
        choosen_spec = input("Enter number: ")

    for option in specialization:
        option.click()
        if option.get_attribute("value") == choosen_spec:
            break

    # set search dates
    if data_dstart and data_dto:
        dates=driver.find_element_by_xpath('/html/body/div[1]/div/div[1]/form/section/div/div[1]/div[2]/div[3]/div/input[1]')
        dates.clear()
        dates.send_keys(str(data_dstart) + " - " + str(data_dto))

    # set english langiage
    if english: 
        eng_tickbox = driver.find_element_by_xpath('//*[@id="ForeignLanguageDoctor"]')
        eng_tickbox.click()

def search_visit(driver, data_dstart, data_dto):
    # try to accept rules
    print("* INF: Entering and filling search pages")
    try:
        tick_button = driver.find_element_by_xpath('//*[@id="AcptRul"]')
        tick_button.click()
    except:
        print("* INF: Cannot find check box - skipping")

    # try to click search button
    print("* INF: \t Filling dates and click search button")
    try:
        # set search dates
        if data_dstart and data_dto:
            dates=driver.find_element_by_xpath('/html/body/div[1]/div/div[1]/form/section/div/div[1]/div[2]/div[3]/div/input[1]')
            dates.clear()
            dates.send_keys(str(data_dstart) + " - " + str(data_dto))
        search_button = driver.find_element_by_xpath('//*[@id="sbtn"]')
        search_button.click()
    except:
        print("* ERR: Cannot navigate on search page - trying again")
        try:
            again_button = driver.find_element_by_xpath('//*[@id="SearchAgain"]')
            again_button.click()
            time.sleep(1)
            search_visit(driver, data_dstart, data_dto)
        except:
            print("* ERR: Cannot navigate on search page")
            raise
    finally:
        print("* INF: Searching ....")

def parse_dates(driver):
    print("* INF: Starting date parser") 
    all_dates_dict = {}
    return_dict = {}
    time.sleep(1)
    print("* INF: \t Search row with visits") 
    try:
        all_rows = driver.find_elements_by_class_name("row.row-search")
    except: 
        print("ERR: Parsing rows with dates failed")
    
    print("* INF: \t Parse dates from ") 
    for dates in all_rows:
        print("* INF: Rows parsing")
        try:
            buton = dates.find_element_by_class_name("btn.btn-primary.makeReservationButton")
            all_dates_dict.update({buton:buton.get_attribute('onclick')})
        except:
            print("Row parsing problem - skipping")    
    
    print("* INF: \t Start regexp to extract dates and other informations ") 
    x = 0
    for variable in all_dates_dict:
        #print("* INF: \t Parse line " + str(variable)) 
        podzielona = all_dates_dict[variable].split(',')
        html=podzielona[7].encode().decode('unicode-escape')
        html=html.replace("\r\n",'')
        lekarz = re.search(r'Lekarz</label></span>[ ]*([a-zA-Z-]* [a-zA-Z-]*).*</p>',html).group(1)
        data = re.search(r'([0-9]{4}-[0-9]{2}-[0-9]{2}) 00:00:00',podzielona[1]).group(1)
        godzina = re.search(r'([0-9]{2}:[0-9]{2}):00',podzielona[2]).group(1)
        return_dict.update({x:{variable:[data,godzina,lekarz]}})
        x=x+1
    return return_dict

def check_booking(visits_dict, start_date, end_date, after_hour, before_hour):
    found = {}
    #print(visits_dict)
    print("* INF: Check visits ...")

    start_date = datetime.strptime(start_date, "%Y-%m-%d")
    end_date = datetime.strptime(end_date, "%Y-%m-%d")
    after_time = datetime.strptime(str(after_hour), "%H:%M")
    before_time = datetime.strptime(str(before_hour), "%H:%M")

    for row in visits_dict.keys():
        #print("Visit row: " + str(row))
        for x in visits_dict[row].values():
            visit_date = datetime.strptime(str(x[0]), "%Y-%m-%d")
            visit_time = datetime.strptime(str(x[1]), "%H:%M")
            print("\t * Available visit: " + visit_date.strftime("%Y-%m-%d") + " " + visit_time.strftime("%H:%M") + " - checking your requirements ...")
            print("\t\t * " + str(visit_date.strftime("%Y-%m-%d")) + " is less or equal than " + str(end_date.strftime("%Y-%m-%d")))
            print("\t\t * " + str(visit_date.strftime("%Y-%m-%d")) + " is greater or equal than " + str(start_date.strftime("%Y-%m-%d")))
            print("\t\t * " + str(visit_time.strftime("%H:%M")) + " is greater or equal than " + str(after_time.strftime("%H:%M")))
            print("\t\t * " + str(visit_time.strftime("%H:%M")) + " is less or equal than " + str(before_time.strftime("%H:%M")))

            if visit_date <= end_date and visit_date >= start_date:
                if visit_time >= after_time and visit_time <= before_time:
                    print("-----------------------------------------------------------")
                    print("* INF: FOUND APPOINTMENT DATE and HOUR: {} {}".format(visit_date.strftime("%Y-%m-%d"),visit_time.strftime('%H:%M')))
                    print("-----------------------------------------------------------")
                    found.update(visits_dict[row])
                else:
                    print("\t\t * Visit dont`t meet hours criteria")
            else:
                print("\t\t * Visit dont`t meet date criteria")
    time.sleep(5)
    return found

def book_visit(driver,book_button):
    try:
        first_reservation_button_reference = list(book_dict.keys())[0]
        first_reservation_button_reference.click()

        time.sleep(2)
        accept_rule = driver.find_element_by_xpath('//*[@id="AcceptedRules"]')
        accept_rule.click()

        time.sleep(1)
        book_button = driver.find_element_by_xpath('//*[@id="confirmReservationButton"]')
        book_button.click()
    except Exception as e:
        print("* ERR: Booking visit failed - " + str(e))
        print("* ERR: Dictionary looks like that -> " + str(list(book_dict.keys())[0]))
    else:
        print("* INF: Booking success - exit")



#---------------------------------------------------------------------------------
# MAIN PROGRAM START HERE 
# --------------------------------------------------------------------------------
if __name__ == '__main__':
    def to_date(value):
        if not re.match('^[0-9]{4}-[0-9]{2}-[0-9]{2}$', value):
            raise argparse.ArgumentTypeError("Invalid date (%s). Should be in YYYY-MM-DD format." % value)
        return value

    def to_hour(value):
        if not re.match('^[0-9]{2}:[0-9]{2}$', value):
            raise argparse.ArgumentTypeError("Invalid hour (%s). Should be in HH-mm format." % value)

        hour = re.search(r'^([0-9]{2})\:([0-9]{2})$',value).group(1)
        minute = re.search(r'^([0-9]{2})\:([0-9]{2})$',value).group(2)
        if int(hour) > 24 or int(hour) < 0 or int(minute) > 60 or int(minute) < 0: 
           raise argparse.ArgumentTypeError("Invalid hour or minute in (%s)." % value)
        return value

    parser = argparse.ArgumentParser()
    parser.add_argument("--login", help="Your Enelmed login", required=True)
    parser.add_argument("--password", help="Your Enelmed password", required=True)
    parser.add_argument("--date_from", help="Enter the start date of the search period e.g 2018-09-10", required=True, type=to_date)
    parser.add_argument("--date_to", help="Enter the end date of the search period e.g 2018-09-12", required=True, type=to_date)
    parser.add_argument("--after_hour", help="Search visits after an hour e.g 08:00", default=DEFAULT_AFTER_HOUR, type=to_hour)
    parser.add_argument("--before_hour", help="Search visits before an hour e.g 22:00", default=DEFAULT_BEFORE_HOUR, type=to_hour)
    parser.add_argument("--check_interval", help="Sleep between searches in secs", type=int, default=DEFAULT_CHECK_INTERVAL_IN_SEC)
    parser.add_argument("--eng_language", help="Pick a english speaking doctor",action='store_true')
    parser.add_argument("--autobook", help="Automate book visit when match",action='store_true')
    parser.add_argument("--nosound", help="Skip msuci play on success",action='store_true')
    args = parser.parse_args()
    browser = webdriver.Firefox()

    # loggin screen
    try:
        login(browser,args.login,args.password)
    except:
        print("* ERR: login failed - exiting")
        exit(1)

    # input values in search visit form - first try
    try:
        choose_visit(browser, args.date_from, args.date_to, args.eng_language)
    except:
        print("* ERR: choose visit failed - exiting")

    try:
        search_visit(browser, args.date_from, args.date_to)
    except:
        print("* ERR: search visit failed - exiting")

    try:
        book_dict = check_booking(parse_dates(browser), args.date_from, args.date_to, args.after_hour, args.before_hour)
    except:
        print("* ERR: book visit failed - exiting")

    # search visit in a loop - is earch visin hang up anter choose visit page again. if visit found play sound
    while not book_dict:
        try:
            time.sleep(args.check_interval)
            search_visit(browser, args.date_from, args.date_to)
        except:
            print("* ERR: search page doesn`t look as it should - trying to fix that")
            choose_visit(browser, args.date_from, args.date_to, args.eng_language)
            search_visit(browser, args.data_from, args.date_to)
        finally:
            book_dict = check_booking(parse_dates(browser), args.date_from, args.date_to, args.after_hour, args.before_hour)
    else:
        browser.maximize_window()
        if args.autobook:
            book_visit(browser, book_dict)

        if args.nosound:
            browser.maximize_window()
            time.sleep(60)
        else:
            p = vlc.MediaPlayer("corona_final.mp3")
            p.play()
            time.sleep(60)
            p.stop()
    browser.close()
