Enelmed Auto Registration Selenium Script
======

Imagine someone resigns from his visit in elenmed. 
To find such an opportunity, you need to be on the site all the time and refresh your search. 
With this script, you can automate this process and find appointments faster, even on the same day.

All you need to do is set your preferred date range, preferred time and doctor's specialty.
Scripts move on the enelmed website and search in loop for the best appointment. The first term found is chosen. 
After finding the preferred visit, script could also register you.

Author and License
=====
Author: pyton

License: I don`t know yet 

ToDo:
=====
 * Check exit code for final request
 * Check english language option

Instalation:
=====
```
git clone https://bitbucket.org/qython/enelmed_autobook.git
cd enelmed_autobook
virtualenv .
source ./bin/activate
pip install -r requirements.txt
```

Usage:
=====
```bash
python3 enelmed_autobook.py \
 --date_from 2018-11-08 \
 --date_to 2018-11-20 \
 --after_hour=18:00 \
 --before_hour=19:00 \
 --login=ENTER_YOUR_LOGIN \
 --password=ENTER_YOUR_PASSWORD \
 --check_interval=40 \
 --autobook
```
Sample above try to book your visit between 2018-11-08 - 2018-11-20. Only visits after 18:00 and before 19:00 
will be consider. Autobook option means to book best matching visit automatically. Without that option script only play sound when find proper appointment. 
It will query enelmed search site every 40 seconds. Other preferences like doctor specialization 
script ask you after logon.
 
Notice: ! DO NOT USE **check_interval < 5** OTHERWISE MEDICAL BLACK OPS SECRET SERVICE KNOCK TO YOUR DOOR !


Side notes:
=====
Sometimes pop-ups with advertisements appear on the website. The script tries to detect it and click it, but sometimes it is necessary to run the script again.

If you do not use the autobook option, after hearing the beep, you have 60 seconds to confirm the visit in the browser that has been maximized

Contribution is welcome.
